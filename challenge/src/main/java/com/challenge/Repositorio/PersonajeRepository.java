package com.challenge.Repositorio;

import com.challenge.Personaje;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonajeRepository extends JpaRepository<Personaje,Integer>{
    
}
